//
//  DACalendarDateCell.h
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DACalendarDateCell : UICollectionViewCell{
    UILabel *_dateLabel;
    UILabel *_eventLabel;
}
@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *eventLabel;

@end
