//
//  DACalendarMonthHeader.m
//  Calendar
//
//  Created by Alexander Dovlatov on 10.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "DACalendarMonthHeader.h"

@implementation DACalendarMonthHeader

-(void)prepareForReuse{
    self.monthLabel.text=nil;
}
-(UILabel *)monthLabel{
    if(!_monthLabel&&![self.subviews containsObject:_monthLabel]){
        _monthLabel = [[UILabel alloc] init];
        [self addSubview:_monthLabel];
        _monthLabel.translatesAutoresizingMaskIntoConstraints=NO;
        NSDictionary *views = @{@"monthLabel":_monthLabel};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[monthLabel]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[monthLabel]|" options:0 metrics:nil views:views]];
    }
    return _monthLabel;
}

-(void)setMonthLabel:(UILabel *)monthLabel{
    _monthLabel=monthLabel;
}



@end
