//
//  View.m
//  CalendarBlack
//
//  Created by Mikhail Baynov on 20/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "View.h"

@implementation View

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    [self setupInit];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupInit];
    }
    return self;
}
- (void)setupInit {
    
    self.layer.cornerRadius = 5;
    self.backgroundColor = [UIColor grayColor];

    UILabel *topLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 35.0f)];
    topLabel.text = @"КАЛЕНДАРЬ";
    topLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:topLabel];
    
    /////test
    //    self.categoriesArraySelected[1] = @YES;
    //    self.categoriesArraySelected[2]=@YES;
    //    ///
    //
    
    self.weekDaysArray = [[NSArray alloc]initWithObjects:@"Пн", @"Вт", @"Ср", @"Чт", @"Пт", @"Сб", @"Вс", nil];
    
    NSMutableArray * weekDayLabelsArray = [[NSMutableArray alloc]init];
    for (int i=0; i<7; i++) {
        UILabel *label = [UILabel new];
        label.text = self.weekDaysArray[i];
        label.frame = CGRectMake(i*self.frame.size.width/8.0f + 25.0f, 47.0f, 20.0f, 20.0f);
        [label setFont:[UIFont fontWithName:@"HelveticaNeue" size:10]];
        [weekDayLabelsArray addObject:label];
        label.textColor = (i<5)?[UIColor blackColor]:[UIColor redColor];
        [self addSubview:label];
    
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
