//
//  AppDelegate.h
//  CalendarBlack
//
//  Created by Mikhail Baynov on 14/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
