//
//  DACalendarExpandedLayout.m
//  Calendar
//
//  Created by Alexander Dovlatov on 06.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "DACalendarExpandedLayout.h"


@implementation DACalendarExpandedLayout


-(void)prepareLayout{
    if(!_dateItemsLayoutAttributes||!_headerViewsLayoutAttributes||!_dayOfWeekViewsLayoutAttributes)[self calculateLayout];
}

-(void)calculateLayout{
    _dateItemsLayoutAttributes = [[NSMutableDictionary alloc] init];
    _headerViewsLayoutAttributes = [[NSMutableDictionary alloc] init];
    _dayOfWeekViewsLayoutAttributes = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *sectionsFrames = [[NSMutableDictionary alloc] init];

    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    float rowContentSizeWidth = self.calendar.bounds.size.width-self.contentInsets.left-self.contentInsets.right-self.sectionInsets.left-self.sectionInsets.right;
    
    float headerHeight = self.headerViewSize.height + self.headerInsets.top + self.headerInsets.bottom;
    float weekDayHeight = self.dayOfWeekCellSize.height>0?self.dayOfWeekCellSize.height+self.rowVerticalSpacing:0;
    float dateItemHeight = self.dateCellSize.height+self.rowVerticalSpacing;
    
    for (NSUInteger sectionIndex=0; sectionIndex<self.calendar.numberOfSections; sectionIndex++) {
        NSIndexPath *sectionIndexPath = [NSIndexPath indexPathWithIndex:sectionIndex];
        
        NSDate *sectionFirstDate = sectionIndex==0?self.firstDay:[[self.firstDay dateByAddingYears:0 months:sectionIndex days:0 hours:0 minutes:0 seconds:0 forNSCalendar:[NSCalendar currentCalendar]] dateByReplacingDay:1 forNSCalendar:[NSCalendar currentCalendar]];
        NSUInteger sectionFirstDateWeekOfMonth = [sectionFirstDate weekOfMonth];
        
        CGRect sectionRect;
        sectionRect.origin.x=self.contentInsets.left+self.sectionInsets.left;
        if(sectionIndex==0){
            sectionRect.origin.y=self.contentInsets.top+self.sectionInsets.top;
        }else{
            CGRect previousSectionRect = [sectionsFrames[[NSIndexPath indexPathWithIndex:sectionIndex-1]] CGRectValue];
            sectionRect.origin.y = previousSectionRect.origin.y+previousSectionRect.size.height;
        }
        
        //Header
        UICollectionViewLayoutAttributes *headerAttributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:DACalendarElementKindMonthHeader withIndexPath:sectionIndexPath];
        headerAttributes.center = CGPointMake((self.calendar.bounds.size.width-self.contentInsets.left-self.contentInsets.right-self.sectionInsets.left-self.contentInsets.right)/2.f, self.sectionInsets.top+self.headerViewSize.height/2.f+sectionRect.origin.y);
        headerAttributes.size=self.headerViewSize;
        [_headerViewsLayoutAttributes setObject:headerAttributes forKey:sectionIndexPath];
        
        //Days of week
//        for (NSUInteger weekDayIndex=calendar.firstWeekday; weekDayIndex<calendar.firstWeekday+7; weekDayIndex++) {
//            NSInteger column = weekDayIndex - calendar.firstWeekday;
//            if(column<0)column+=7;
        
            
//            NSIndexPath *weekDayIndexPath = [NSIndexPath indexPathForItem:column inSection:sectionIndex];
            
            
//            UICollectionViewLayoutAttributes *dayOfWeekAttributes = [UICollectionViewLayoutAttributes layoutAttributesForSupplementaryViewOfKind:DACalendarElementKindDayOfWeek withIndexPath:weekDayIndexPath];
//            dayOfWeekAttributes.center =
//            CGPointMake(sectionRect.origin.x + (column+1) * rowContentSizeWidth / 8,
//                        sectionRect.origin.y +
//                        headerHeight +
//                        self.dayOfWeekCellSize.height/2.f);
//            dayOfWeekAttributes.size = self.dayOfWeekCellSize;
//            [_dayOfWeekViewsLayoutAttributes setObject:dayOfWeekAttributes forKey:weekDayIndexPath];
//        }

        
        
        //Items
        CGSize sectionSize=CGSizeZero;
        for (NSUInteger itemIndex=0; itemIndex<[self.calendar numberOfItemsInSection:sectionIndex]; itemIndex++) {
            NSIndexPath *itemIndexPath = [NSIndexPath indexPathForItem:itemIndex inSection:sectionIndex];

            NSDate *currentDate = [sectionFirstDate dateByAddingYears:0 months:0 days:itemIndex hours:0 minutes:0 seconds:0 forNSCalendar:[NSCalendar currentCalendar]];
            NSInteger row = [currentDate weekOfMonth]-sectionFirstDateWeekOfMonth;
            NSInteger column = [currentDate weekday]-calendar.firstWeekday; if(column<0)column+=7;
            
            UICollectionViewLayoutAttributes *itemAttributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:itemIndexPath];
            itemAttributes.center =
            CGPointMake(sectionRect.origin.x + (column+1) * rowContentSizeWidth / 8,
                        
                        sectionRect.origin.y +
                        headerHeight +
                        weekDayHeight +
                        row * dateItemHeight +
                        self.dateCellSize.height/2.f -
                        self.rowVerticalSpacing);
            itemAttributes.size = self.dateCellSize;
            
            [_dateItemsLayoutAttributes setObject:itemAttributes forKey:itemIndexPath];
            sectionSize = CGSizeMake(itemAttributes.frame.origin.x + itemAttributes.size.width - sectionRect.origin.x, itemAttributes.frame.origin.y + itemAttributes.size.height - sectionRect.origin.y + self.sectionInsets.bottom);
        }
        sectionRect.size=sectionSize;
        [sectionsFrames setObject:[NSValue valueWithCGRect:sectionRect] forKey:sectionIndexPath];
        _contentSize = CGSizeMake(MAX(_contentSize.width, sectionRect.size.width+sectionRect.origin.x+self.sectionInsets.right+self.contentInsets.right), MAX(_contentSize.height, sectionRect.origin.y+sectionRect.size.height+self.sectionInsets.bottom+self.contentInsets.bottom));
    }
}

-(void)invalidateLayout{
    _dateItemsLayoutAttributes=_headerViewsLayoutAttributes=_dayOfWeekViewsLayoutAttributes=nil;
    [super invalidateLayout];
    
}


-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    NSMutableArray *attributesArray = [[NSMutableArray alloc] init];
    [_dateItemsLayoutAttributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        UICollectionViewLayoutAttributes *attributes = obj;
        if(CGRectIntersectsRect(rect, attributes.frame)) [attributesArray addObject:obj];
    }];
    
    [_headerViewsLayoutAttributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        UICollectionViewLayoutAttributes *attributes = obj;
        if(CGRectIntersectsRect(rect, attributes.frame)) [attributesArray addObject:obj];
    }];
    [_dayOfWeekViewsLayoutAttributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        UICollectionViewLayoutAttributes *attributes = obj;
        if(CGRectIntersectsRect(rect, attributes.frame)){
            [attributesArray addObject:obj];
        }
    }];
    return attributesArray;
}
-(UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewLayoutAttributes *attributes = _dateItemsLayoutAttributes[indexPath];
    return attributes;
}
-(CGSize)collectionViewContentSize{
    return _contentSize;
}
-(UICollectionViewLayoutAttributes *)layoutAttributesForSupplementaryViewOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if([kind isEqualToString:DACalendarElementKindMonthHeader]){
        return _headerViewsLayoutAttributes[indexPath];
//    } else if ([kind isEqualToString:DACalendarElementKindDayOfWeek]){
//        return _dayOfWeekViewsLayoutAttributes[indexPath];
    }
    else return nil;
}



-(void)prepareForTransitionToLayout:(UICollectionViewLayout *)newLayout{
    if([newLayout isKindOfClass:NSClassFromString(@"DACalendarCollapsedLayout")]){
        
    }
}

-(UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    UICollectionViewLayoutAttributes *currentAttributes = _dateItemsLayoutAttributes[itemIndexPath];
    currentAttributes.alpha=1.f;
    return currentAttributes;
}

-(UICollectionViewLayoutAttributes *)finalLayoutAttributesForDisappearingItemAtIndexPath:(NSIndexPath *)itemIndexPath{
    UICollectionViewLayoutAttributes *currentAttributes = _dateItemsLayoutAttributes[itemIndexPath];
    currentAttributes.alpha=0.f;
    return currentAttributes;
}
@end
