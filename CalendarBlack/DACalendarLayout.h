//
//  DACalendarLayout.h
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSDate+operations.h"
#import "DACalendar.h"

@interface DACalendarLayout : UICollectionViewLayout{
    NSMutableDictionary *_dateItemsLayoutAttributes;
    NSMutableDictionary *_dayOfWeekViewsLayoutAttributes;
    NSMutableDictionary *_headerViewsLayoutAttributes;
    CGSize _contentSize;
}

@property (readonly, weak) DACalendar *calendar;

@property (readonly, weak) NSDate *firstDay, *lastDay;

@property (readonly) CGSize headerViewSize;
@property (readonly) CGSize dayOfWeekCellSize;
@property (readonly) CGSize dateCellSize;

@property (readonly) UIEdgeInsets contentInsets;
@property (readonly) UIEdgeInsets headerInsets;
@property (readonly) UIEdgeInsets sectionInsets;
@property (readonly) float rowVerticalSpacing;

@end
