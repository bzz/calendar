//
//  NSDate+operations.h
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (operations)
-(NSDate*)dateByAddingYears:(NSInteger)years months:(NSInteger)months days:(NSInteger)days hours:(NSInteger)hours minutes:(NSInteger)minutes seconds:(NSInteger)seconds forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingDay:(NSInteger)day forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingMonth:(NSInteger)month forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingYear:(NSInteger)year forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingHour:(NSInteger)hour forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingMinute:(NSInteger)minute forNSCalendar:(NSCalendar*)calendar;
-(NSDate*)dateByReplacingSecond:(NSInteger)second forNSCalendar:(NSCalendar*)calendar;

@property (readonly) NSUInteger day, month, year, hour, minute, second, weekday, weekOfMonth;
@end
