//
//  NSDate+operations.m
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "NSDate+operations.h"

@implementation NSDate (operations)
-(NSDate*)dateByAddingYears:(NSInteger)years months:(NSInteger)months days:(NSInteger)days hours:(NSInteger)hours minutes:(NSInteger)minutes seconds:(NSInteger)seconds forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year=years;
    components.month=months;
    components.day=days;
    components.hour=hours;
    components.minute=minutes;
    components.second=seconds;
    
    return [calendar dateByAddingComponents:components toDate:self options:0];
}
-(NSDate*)dateByReplacingDay:(NSInteger)day forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.day=day;
    return [calendar dateFromComponents:components];
}
-(NSDate*)dateByReplacingMonth:(NSInteger)month forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.month=month;
    return [calendar dateFromComponents:components];
}
-(NSDate*)dateByReplacingYear:(NSInteger)year forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.year=year;
    return [calendar dateFromComponents:components];
}
-(NSDate*)dateByReplacingHour:(NSInteger)hour forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.hour=hour;
    return [calendar dateFromComponents:components];
}
-(NSDate*)dateByReplacingMinute:(NSInteger)minute forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.minute=minute;
    return [calendar dateFromComponents:components];
}
-(NSDate*)dateByReplacingSecond:(NSInteger)second forNSCalendar:(NSCalendar*)calendar{
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit) fromDate:self];
    components.second=second;
    return [calendar dateFromComponents:components];
}
-(NSUInteger)day{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit) fromDate:self];
    return components.day;
}
-(NSUInteger)month{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSMonthCalendarUnit) fromDate:self];
    return components.month;
}
-(NSUInteger)year{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit) fromDate:self];
    return components.year;
}
-(NSUInteger)hour{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit) fromDate:self];
    return components.hour;
}
-(NSUInteger)minute{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSMinuteCalendarUnit) fromDate:self];
    return components.minute;
}
-(NSUInteger)second{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSSecondCalendarUnit) fromDate:self];
    return components.second;
}
-(NSUInteger)weekday{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSWeekdayCalendarUnit) fromDate:self];
    return components.weekday;
}
-(NSUInteger)weekOfMonth{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSWeekOfMonthCalendarUnit) fromDate:self];
    return components.weekOfMonth;
}
@end
