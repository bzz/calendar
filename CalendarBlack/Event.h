//
//  Event.h
//  CalendarBlack
//
//  Created by Mikhail Baynov on 17/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject
@property (strong, nonatomic) NSString *name;
@property                     NSUInteger type;
@property (strong, nonatomic) NSDate   *date;
@property (strong, nonatomic) NSString *key;
//@property (strong, nonatomic) NSString *description;
//@property (strong, nonatomic) UIImage  *image;



- (Event*)initWithEventNamed:(NSString*)name ofType:(NSUInteger)type happeningOnDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year forCalendar:(NSCalendar*)calendar;


@end
