//
//  UIImage+PixelData.m
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import "UIImage+PixelData.h"

@implementation UIImage (PixelData)

///Gets image from pixel data with size of image
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size {
	CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return image;
}

+ (UIImage *)imageBySplittingTop:(UIImage *)image0 pixels:(CGFloat)pixels {
    CGImageRef tmpImgRef = image0.CGImage;
    CGImageRef topImgRef = CGImageCreateWithImageInRect(tmpImgRef, CGRectMake(0.0f, 0.0f, image0.size.width, pixels));
    UIImage *topImage = [UIImage imageWithCGImage:topImgRef];
    return topImage;
}


+ (UIImage *)imageBySplittingBottom:(UIImage *)image0 pixels:(CGFloat)pixels {
    CGImageRef tmpImgRef = image0.CGImage;
    CGImageRef bottomImgRef = CGImageCreateWithImageInRect(tmpImgRef, CGRectMake(0.0f, pixels, image0.size.width, image0.size.height-pixels));
    UIImage *bottomImage = [UIImage imageWithCGImage:bottomImgRef];
    return bottomImage;
}


@end
