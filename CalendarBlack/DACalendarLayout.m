//
//  DACalendarLayout.m
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "DACalendarLayout.h"

@implementation DACalendarLayout
@synthesize rowVerticalSpacing;
@synthesize contentInsets, headerInsets, sectionInsets;

-(DACalendar *)calendar{return (DACalendar *)self.collectionView;}
-(NSDate *)firstDay{return self.calendar.firstDay;}
-(NSDate *)lastDay{return self.calendar.lastDay;}

-(CGSize)dateCellSize{
    return [self.calendar.calendarDatasource calendar:self.calendar dateItemSizeForLayout:self];
}

-(CGSize)headerViewSize{
    if(self.calendar.calendarDatasource&&[self.calendar.calendarDatasource respondsToSelector:@selector(calendar:monthHeaderViewSizeForLayout:)])
        return [self.calendar.calendarDatasource calendar:self.calendar monthHeaderViewSizeForLayout:self];
    else return CGSizeZero;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

-(UIEdgeInsets)contentInsets{
    if(self.calendar.calendarDatasource&&[self.calendar.calendarDatasource respondsToSelector:
                                          @selector(calendar:contentInsetsForLayout:)])
        return [self.calendar.calendarDatasource calendar:self.calendar contentInsetsForLayout:self];
    else return UIEdgeInsetsZero;
}
-(UIEdgeInsets)headerInsets{
    if(self.calendar.calendarDatasource&&[self.calendar.calendarDatasource respondsToSelector:
                                          @selector(calendar:headerInsetsForLayout:)])
        return [self.calendar.calendarDatasource calendar:self.calendar headerInsetsForLayout:self];
    else return UIEdgeInsetsZero;
}
-(UIEdgeInsets)sectionInsets{
    if(self.calendar.calendarDatasource&&[self.calendar.calendarDatasource respondsToSelector:
                                          @selector(calendar:sectionInsetsForLayout:)])
        return [self.calendar.calendarDatasource calendar:self.calendar sectionInsetsForLayout:self];
    else return UIEdgeInsetsZero;
}
-(float)rowVerticalSpacing{
    if(self.calendar.calendarDatasource&&[self.calendar.calendarDatasource respondsToSelector:
                                          @selector(calendar:rowVerticalSpacingForLayout:)])
        return [self.calendar.calendarDatasource calendar:self.calendar rowVerticalSpacingForLayout:self];
    else return 0;
}

@end
