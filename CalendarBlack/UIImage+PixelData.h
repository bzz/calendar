//
//  UIImage+PixelData.h
//  ArtStudio
//
//  Created by Mikhail Baynov on 11/25/13.
//  Copyright (c) 2013 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (PixelData)

+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;
+ (UIImage *)imageBySplittingTop:(UIImage *)image pixels:(CGFloat)pixels;
+ (UIImage *)imageBySplittingBottom:(UIImage *)image pixels:(CGFloat)pixels;


@end
