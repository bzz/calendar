//
//  Event.m
//  CalendarBlack
//
//  Created by Mikhail Baynov on 17/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import "Event.h"

@implementation Event


- (Event*)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (Event*)initWithEventNamed:(NSString*)name ofType:(NSUInteger)type happeningOnDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year forCalendar:(NSCalendar*)calendar {
    self = [super init];
    if (self) {
        self.name = name;
        self.type = type;
        if (!calendar) {
            calendar = [NSCalendar currentCalendar];
        }
        NSDateComponents *components = [[NSDateComponents alloc] init];
        [components setYear:year];
        [components setMonth:month];
        [components setDay:day];
        self.date = [calendar dateFromComponents:components];
        
        NSDateFormatter *universalFormatter = [[NSDateFormatter alloc] init];
        universalFormatter.dateFormat=@"dd.MM.YYYY";
        [universalFormatter setTimeZone:[NSTimeZone localTimeZone]];
        self.key = [universalFormatter stringFromDate:self.date];
    }
    return self;
}

@end