//
//  DACalendar.h
//  Calendar
//
//  Created by Alexander Dovlatov on 06.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//TODO:REMOVE!
#import "DACalendarDateCell.h"
#import "NSDate+operations.h"

#import "DACalendarDatasourceProtocol.h"

typedef enum {DACalendarLayoutStateExpanded=0, DACalendarLayoutStateCollapsed} DACalendarLayoutState;
//#define DACalendarElementKindDayOfWeek @"kCalendarElementKindDayOfWeek"
#define DACalendarElementKindMonthHeader @"kCalendarElementKindMonthHeader"


@class DACalendarLayout, DACalendarExpandedLayout, DACalendarCollapsedLayout;

@interface DACalendar : UICollectionView<UICollectionViewDataSource>{
    DACalendarLayoutState _state;
    UICollectionViewLayout *_expandedLayout, *_collapsedLayout;
    NSDate *_firstDay, *_lastDay;
}
@property NSDate *firstDay, *lastDay;
@property (readonly, weak) DACalendarLayout *calendarLayout;


-(void)registerItemClass:(Class)cellClass forLayoutClass:(Class)layoutClass;
//-(void)registerDayOfWeekItemClass:(Class)cellClass forLayoutClass:(Class)layoutClass;
-(void)registerHeaderClass:(Class)viewClass forLayoutClass:(Class)layoutClass;
-(id)dequeueReusableItemForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass;
//-(id)dequeueDayOfWeekItemForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass;
-(id)dequeueReusableHeaderViewForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass;

@property id <DACalendarDatasourceProtocol> calendarDatasource;
@end
