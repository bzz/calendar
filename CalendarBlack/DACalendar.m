//
//  DACalendar.m
//  Calendar
//
//  Created by Alexander Dovlatov on 06.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "DACalendar.h"
#import "DACalendarExpandedLayout.h"

@implementation DACalendar
@synthesize calendarDatasource;



#pragma mark
#pragma mark -=Initialization=-
- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark
#pragma mark -=Setup=-
-(void)setup{
    self.dataSource=self;
    
}
#pragma mark
#pragma mark -=Properties=-

////First day
-(void)setFirstDay:(NSDate *)firstDay{_firstDay=firstDay; [self.collectionViewLayout invalidateLayout];}
-(NSDate *)firstDay{return _firstDay;}
////Last day
-(void)setLastDay:(NSDate *)lastDay{_lastDay=lastDay;}
-(NSDate *)lastDay{return _lastDay; [self.collectionViewLayout invalidateLayout];}

//State
-(UICollectionViewLayout*)expandedLayout{
    if(!_expandedLayout)_expandedLayout= [[DACalendarExpandedLayout alloc] init];
    return _expandedLayout;
}


-(DACalendarLayout *)calendarLayout{
    return (DACalendarLayout*)self.collectionViewLayout;
}


-(void)setDataSource:(id<UICollectionViewDataSource>)dataSource{[super setDataSource:self];}

#pragma mark
#pragma mark -=Classes registration & dequeue=-
-(void)registerItemClass:(Class)cellClass forLayoutClass:(Class)layoutClass{
    [self registerClass:cellClass forCellWithReuseIdentifier:[[NSString alloc] initWithFormat:@"%@~dateCell", NSStringFromClass(layoutClass)]];
}
//-(void)registerDayOfWeekItemClass:(Class)cellClass forLayoutClass:(Class)layoutClass{
//    NSString *reuseIdentifier = [[NSString alloc] initWithFormat:@"%@~%@", NSStringFromClass(layoutClass), DACalendarElementKindDayOfWeek];
//    [self registerClass:cellClass forSupplementaryViewOfKind:DACalendarElementKindDayOfWeek withReuseIdentifier:reuseIdentifier];
//}


-(void)registerHeaderClass:(Class)viewClass forLayoutClass:(Class)layoutClass{
    NSString *reuseIdentifier = [[NSString alloc] initWithFormat:@"%@~%@", NSStringFromClass(layoutClass), DACalendarElementKindMonthHeader];
    [self registerClass:viewClass forSupplementaryViewOfKind:DACalendarElementKindMonthHeader withReuseIdentifier:reuseIdentifier];
}




//Dequeue
-(id)dequeueReusableItemForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass{
    return [self dequeueReusableCellWithReuseIdentifier:[[NSString alloc] initWithFormat:@"%@~dateCell", NSStringFromClass(layoutClass)] forIndexPath:indexPath];
}

//-(id)dequeueDayOfWeekItemForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass{
//    NSString *reuseIdentifier = [[NSString alloc] initWithFormat:@"%@~%@", NSStringFromClass(layoutClass), DACalendarElementKindDayOfWeek];
//    
//    return [self dequeueReusableSupplementaryViewOfKind:DACalendarElementKindDayOfWeek withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
//}

-(id)dequeueReusableHeaderViewForIndexPath:(NSIndexPath *)indexPath forLayoutClass:(Class)layoutClass{
    NSString *reuseIdentifier = [[NSString alloc] initWithFormat:@"%@~%@", NSStringFromClass(layoutClass), DACalendarElementKindMonthHeader];
    
    return [self dequeueReusableSupplementaryViewOfKind:DACalendarElementKindMonthHeader withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
}

#pragma mark
#pragma mark -=Data source=-
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self.calendarDatasource calendar:self dateItemAtIndexPath:indexPath forDate:[self dateForIndexPath:indexPath] forLayout:self.calendarLayout];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    if(section==0){
        NSRange daysInMonthRange = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:self.firstDay];
        return daysInMonthRange.length + 1 - self.firstDay.day;
    }
    if(section==[self numberOfSectionsInCollectionView:self]-1){
        return self.lastDay.day - 1;
    }
    else{
        NSDate *date = [self dateForIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
        NSRange daysInMonthRange = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
        return daysInMonthRange.length;
    }
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    NSDate *date = [self dateForIndexPath:[NSIndexPath indexPathForItem:1 inSection:indexPath.section]];
    if([kind isEqualToString:DACalendarElementKindMonthHeader]){
        if(self.calendarDatasource&&[self.calendarDatasource respondsToSelector:@selector(calendar:monthHeaderViewAtIndexPath:forDate:forLayout:)])
            return [self.calendarDatasource calendar:self monthHeaderViewAtIndexPath:indexPath forDate:date forLayout:self.calendarLayout];
    }

    
    return nil;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSMonthCalendarUnit
                                                   fromDate:self.firstDay
                                                     toDate:self.lastDay
                                                    options:0];

//    NSCalendarCalendarUnit
    NSUInteger sectionsCount = dateComponents.month;
    if(self.firstDay.day>0)sectionsCount++;
    return sectionsCount;
}

#pragma mark
#pragma mark -=Helpers=-
-(NSDate*)dateForIndexPath:(NSIndexPath*)indexPath{
    NSDate *date = [[self.firstDay dateByReplacingDay:1 forNSCalendar:[NSCalendar currentCalendar]] dateByAddingYears:0 months:indexPath.section days:indexPath.item hours:0 minutes:0 seconds:0 forNSCalendar:[NSCalendar currentCalendar]];
    if(indexPath.section==0)
        date = [date dateByAddingYears:0 months:0 days:self.firstDay.day-1 hours:0 minutes:0 seconds:0 forNSCalendar:[NSCalendar currentCalendar]];
    else
        date = [date dateByReplacingDay:(indexPath.item+1) forNSCalendar:[NSCalendar currentCalendar]];
    
    return date;
}

#pragma mark
#pragma mark -=Overrides=-

@end
