//
//  DACalendarMonthHeader.h
//  Calendar
//
//  Created by Alexander Dovlatov on 10.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DACalendarMonthHeader : UICollectionReusableView{
    UILabel *_monthLabel;
}
@property IBOutlet UILabel *monthLabel;
@end
