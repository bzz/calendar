//
//  DACalendarDateCell.m
//  Calendar
//
//  Created by Alexander Dovlatov on 07.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import "DACalendarDateCell.h"

@implementation DACalendarDateCell

-(void)prepareForReuse{
    self.dateLabel.text=nil;
    self.dateLabel.backgroundColor=[UIColor clearColor];
    self.dateLabel.textColor = [UIColor blackColor];
    self.eventLabel.text=nil;
    self.eventLabel.backgroundColor=[UIColor clearColor];
    self.eventLabel.textColor = [UIColor blackColor];
}
-(UILabel *)dateLabel{
    if(!_dateLabel&&![self.subviews containsObject:_dateLabel]){
        _dateLabel = [[UILabel alloc] init];
        [self addSubview:_dateLabel];
        _dateLabel.translatesAutoresizingMaskIntoConstraints=NO;
        NSDictionary *views = @{@"dateLabel":_dateLabel};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[dateLabel]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[dateLabel]|" options:0 metrics:nil views:views]];
    }
    return _dateLabel;
}
-(void)setDateLabel:(UILabel *)dateLabel{
    _dateLabel=dateLabel;
}

-(UILabel *)eventLabel{
    if(!_eventLabel&&![self.subviews containsObject:_eventLabel]){
        _eventLabel = [[UILabel alloc] init];
        [self addSubview:_eventLabel];
        _eventLabel.translatesAutoresizingMaskIntoConstraints=NO;
        NSDictionary *views = @{@"eventLabel":_eventLabel};
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[eventLabel]|" options:0 metrics:nil views:views]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[eventLabel]|" options:0 metrics:nil views:views]];
    }
    return _eventLabel;
}

-(void)setEventLabel:(UILabel *)eventLabel{
    _eventLabel=eventLabel;
}


@end
