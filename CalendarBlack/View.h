//
//  View.h
//  CalendarBlack
//
//  Created by Mikhail Baynov on 20/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface View : UIView
@property (strong, nonatomic) NSArray *weekDaysArray;

@end
