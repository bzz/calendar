//
//  ViewController.m
//  CalendarBlack
//
//  Created by Mikhail Baynov on 14/02/14.
//  Copyright (c) 2014 Mikhail Baynov. All rights reserved.
//


#define LEFT_CALENDAR_OFFSET 50
#define TOP_CALENDAR_TITLE_OFFSET 70
#define BOTTOM_CATEGORIES_BUTTON_SIZE 50
#define TABLE_CELL_HEIGHT 50.0f
#define BUTTON_LINK_LENGTH 150.0f
#define BOTTOM_TABLE_HEIGHT 250.0f




#import "ViewController.h"

@interface ViewController () {
    UIAttachmentBehavior *dragBehaviour;
    UIPanGestureRecognizer * panGR;
}
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSDateFormatter *monthFormatter;
@property (strong, nonatomic) NSDateFormatter *dayOfWeekFormatter;
@property (strong, nonatomic) NSDateFormatter *universalFormatter;

//@property (strong, nonatomic) NSMutableArray *eventsArray;
@property (strong, nonatomic) NSMutableDictionary *eventsDictionary;
@property (strong, nonatomic) NSMutableDictionary *eventsDictionaryAllEvents;
@property (strong, nonatomic) View *mainView;
@property (strong, nonatomic) DACalendar *calendar;
//@property (strong, nonatomic) UIView *categoriesView;
@property (strong, nonatomic) UITableView *categoriesTableView;

@property (strong, nonatomic) UIView *categoriesButtonView;
@property (strong, nonatomic) NSCalendar *calendarNSCalendar;
@property BOOL isCategoriesListVisible;
@property (strong, nonatomic) NSArray *categoriesArray;
@property (strong, nonatomic) NSMutableArray *categoriesArraySelected;
@property (strong, nonatomic) NSMutableArray *categorizedEventsArray;
@property float heightForRowAtIndexPath;
@property (strong, nonatomic) UIDynamicAnimator* animator;


@end

@implementation ViewController

#pragma mark -
#pragma mark INITIAL SETUP

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupMainView];
    [self setupArrays];
    [self setupCalendarAndEvents];
    [self setupButtons];
    [self setupCategoriesView];
    [self setupDateFormattersAndCalendar];
    [self registerItemClassesForLayoutClasses];
    
    [self.categoriesTableView setContentSize:CGSizeMake(self.mainView.frame.size.width, self.categoriesArray.count * TABLE_CELL_HEIGHT)];
}

- (void)setupMainView {
    self.mainView = [[View alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+LEFT_CALENDAR_OFFSET, self.view.frame.origin.y, self.view.frame.size.width -LEFT_CALENDAR_OFFSET, self.view.frame.size.height)];
    [self.view addSubview:self.mainView];
}

- (void)setupArrays {
    self.categoriesArray = [[NSArray alloc]initWithObjects:@"Балет", @"Выставки", @"Кино", @"Театры", @"Опера", @"Балет1", @"Выставки1", @"Кино1", @"Театры1", @"Опера1", nil];
    self.categoriesArraySelected = [[NSMutableArray alloc]initWithCapacity:self.categoriesArray.count];
    for (NSInteger i = 0; i<self.categoriesArray.count; i++) {
        self.categoriesArraySelected[i] = @NO;
    }

}

- (void)setupCalendarAndEvents {
    self.calendarNSCalendar = [NSCalendar currentCalendar];
    self.calendarNSCalendar.firstWeekday = 2;
    
    NSMutableArray *eventsArray = [[NSMutableArray alloc]init];
    
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event0" ofType:0 happeningOnDay:23 month:2 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event1" ofType:1 happeningOnDay:8 month:3 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event2" ofType:2 happeningOnDay:15 month:3 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event3" ofType:3 happeningOnDay:16 month:3 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event4" ofType:4 happeningOnDay:20 month:3 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event5" ofType:5 happeningOnDay:20 month:4 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event6" ofType:6 happeningOnDay:21 month:4 year:2014 forCalendar:self.calendarNSCalendar]];
    [eventsArray addObject:[[Event alloc]initWithEventNamed:@"Event7" ofType:7 happeningOnDay:1 month:5 year:2014 forCalendar:self.calendarNSCalendar]];
   
    
    NSUInteger max = 0;
    for (Event *event in eventsArray) {
        if (event.type > max) {
            max = event.type;
        }
    }
    self.categorizedEventsArray = [[NSMutableArray alloc]initWithCapacity:max+1];
    for (NSUInteger i = 0; i<max+1; i++) {
        NSMutableArray *array = [[NSMutableArray alloc]init];
        self.categorizedEventsArray[i]=array;
    }
    
    
    for (Event *event in eventsArray) {
        [self.categorizedEventsArray[event.type] addObject:event];
    }
    
    [self formEventsDictionary];
    [self formEventsDictionaryAllEvents];
    
    
}




- (void)setupButtons {
    self.categoriesButtonView = [[UIView alloc]init];
//    [self.categoriesButton setTitle:@"КАТЕГОРИИ" forState:UIControlStateNormal];
    self.categoriesButtonView.frame = CGRectMake(0, self.mainView.frame.size.height-BOTTOM_CATEGORIES_BUTTON_SIZE, self.mainView.frame.size.width, self.mainView.frame.size.height);
    [self.mainView addSubview:self.categoriesButtonView];
    
    UILabel *categoriesTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.categoriesButtonView.frame.size.width, 50.0f)];
    categoriesTitleLabel.backgroundColor = [UIColor whiteColor];
    categoriesTitleLabel.alpha = 0.5;
    categoriesTitleLabel.textColor = [UIColor greenColor];
    categoriesTitleLabel.text = @"КАТЕГОРИИ";
    categoriesTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self.categoriesButtonView addSubview:categoriesTitleLabel];

    self.categoriesButtonView.tintColor = [UIColor greenColor];
    panGR = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.categoriesButtonView addGestureRecognizer:panGR];
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.mainView];
}

- (void)setupCategoriesView {
    //    self.categoriesView = [[UIView alloc]initWithFrame:CGRectMake(0, self.mainView.frame.size.height-BOTTOM_CATEGORIES_BUTTON_SIZE, self.mainView.frame.size.width, BOTTOM_CATEGORIES_BUTTON_SIZE)];
    self.categoriesTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, BOTTOM_CATEGORIES_BUTTON_SIZE, self.mainView.frame.size.width, BOTTOM_TABLE_HEIGHT) ];
    self.categoriesTableView.delegate = self;
    self.categoriesTableView.dataSource = self;
    [self.categoriesTableView setClipsToBounds:YES];
    [self.categoriesTableView setAllowsMultipleSelection:YES];
    
    //    self.categoriesView.backgroundColor = [UIColor orangeColor];
    self.categoriesTableView.backgroundColor = [UIColor redColor];
    //    [self.mainView addSubview:self.categoriesView];
    [self.categoriesButtonView addSubview:self.categoriesTableView];
}

- (void)setupDateFormattersAndCalendar {
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.monthFormatter = [[NSDateFormatter alloc] init];
    self.dayOfWeekFormatter = [[NSDateFormatter alloc] init];
    self.universalFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat=@"dd";
    self.monthFormatter.dateFormat=@"LLLL";
    self.dayOfWeekFormatter.dateFormat=@"eee";
    self.universalFormatter.dateFormat=@"dd.MM.YYYY";
    [self.dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [self.monthFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [self.dayOfWeekFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [self.universalFormatter setTimeZone:[NSTimeZone localTimeZone]];
    
    
    DACalendarExpandedLayout *l = [[DACalendarExpandedLayout alloc]init];
    self.calendar = [[DACalendar alloc]initWithFrame:CGRectZero collectionViewLayout:l];
    self.calendar.delegate = self;
    self.calendar.calendarDatasource=self;
    self.calendar.firstDay=[[NSDate date] dateByReplacingDay:1 forNSCalendar:self.calendarNSCalendar];
    self.calendar.lastDay=[self.calendar.firstDay dateByAddingYears:1 months:0 days:0 hours:0 minutes:0 seconds:0 forNSCalendar:self.calendarNSCalendar];
    self.calendar.backgroundColor = [UIColor darkGrayColor];
    [self.mainView addSubview:self.calendar];
    self.calendar.frame = CGRectMake(0, +TOP_CALENDAR_TITLE_OFFSET, self.mainView.frame.size.width, self.mainView.frame.size.height-TOP_CALENDAR_TITLE_OFFSET-BOTTOM_CATEGORIES_BUTTON_SIZE);
}

- (void)registerItemClassesForLayoutClasses {
    [self.calendar registerItemClass:[DACalendarDateCell class] forLayoutClass:[DACalendarExpandedLayout class]];
    //    [self.calendar registerDayOfWeekItemClass:[DACalendarDayOfWeekView class] forLayoutClass:[DACalendarExpandedLayout class]];
    [self.calendar registerHeaderClass:[DACalendarMonthHeader class] forLayoutClass:[DACalendarExpandedLayout class]];
}


#pragma mark -
#pragma mark LIFECYCLE

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //    [self.calendar reloadData];
}


- (void)formEventsDictionary {
    if (!self.eventsDictionary) {
        self.eventsDictionary = [[NSMutableDictionary alloc]init];
    }
    [self.eventsDictionary removeAllObjects];
    for (NSInteger i = 0; i<self.categorizedEventsArray.count; i++) {
        if ([self.categoriesArraySelected[i] isEqual:@YES]) {
            for (Event *event in self.categorizedEventsArray[i]) {
                [self.eventsDictionary setObject:event forKey:event.key];
            }
        }
    }
}

- (void)formEventsDictionaryAllEvents {
    if (!self.eventsDictionaryAllEvents) {
        self.eventsDictionaryAllEvents = [[NSMutableDictionary alloc]init];
    }
    [self.eventsDictionaryAllEvents removeAllObjects];
    for (NSInteger i = 0; i<self.categorizedEventsArray.count; i++) {
            for (Event *event in self.categorizedEventsArray[i]) {
                [self.eventsDictionaryAllEvents setObject:event forKey:event.key];
            }
    }
}


#pragma mark -
#pragma mark ACTIONS



- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    [self.mainView bringSubviewToFront:self.categoriesTableView];
    [self.mainView bringSubviewToFront:self.categoriesButtonView];
    CGPoint translation = [recognizer translationInView:self.mainView];
    recognizer.view.center = CGPointMake(recognizer.view.center.x,
                                         recognizer.view.center.y + translation.y);

    [recognizer setTranslation:CGPointZero inView:self.mainView];

    
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
    } else if (recognizer.state == UIGestureRecognizerStateEnded) {
//        [self.animator removeAllBehaviors];
        if (self.mainView.frame.size.height - [recognizer locationInView:self.mainView].y >BOTTOM_TABLE_HEIGHT/2) {
            [self animateSnapUp];
        } else {
            [self animateSnapDown];
        }
    }
}


#pragma mark -
#pragma mark CALENDAR DELEGATE


-(UICollectionViewCell *)calendar:(DACalendar *)calendar dateItemAtIndexPath:(NSIndexPath *)indexPath forDate:(NSDate *)date forLayout:(DACalendarLayout *)layout{
    DACalendarDateCell *dateCell = [calendar dequeueReusableItemForIndexPath:indexPath forLayoutClass:[layout class]];
    dateCell.dateLabel.textAlignment=NSTextAlignmentCenter;
    dateCell.dateLabel.text=[self.dateFormatter stringFromDate:date];
    
    
    if ([self.eventsDictionaryAllEvents objectForKey:[self.universalFormatter stringFromDate:date]]) {
        dateCell.eventLabel.text = @".";
        dateCell.eventLabel.textColor = [UIColor grayColor];
    }
    if ([self.eventsDictionary objectForKey:[self.universalFormatter stringFromDate:date]]) {
        dateCell.dateLabel.textColor = [UIColor greenColor];
        dateCell.eventLabel.textColor = [UIColor greenColor];
        dateCell.eventLabel.text = @".";
    }
    return dateCell;
}

-(CGSize)calendar:(DACalendar *)calendar dateItemSizeForLayout:(DACalendarLayout *)layout{
    return CGSizeMake(25, 25);
}

-(UICollectionReusableView *)calendar:(DACalendar *)calendar monthHeaderViewAtIndexPath:(NSIndexPath *)indexPath forDate:(NSDate *)date forLayout:(DACalendarLayout *)layout{
    DACalendarMonthHeader *view = [calendar dequeueReusableHeaderViewForIndexPath:indexPath forLayoutClass:[layout class]];
    view.monthLabel.textAlignment=NSTextAlignmentCenter;
    view.monthLabel.text = [[self monthFormatter] stringFromDate:date];
    view.monthLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:15.0f];
    //    view.backgroundColor = [UIColor greenColor];
    return view;
}

-(CGSize)calendar:(DACalendar *)calendar monthHeaderViewSizeForLayout:(DACalendarLayout *)layout{
    return CGSizeMake(250.0f, 50.0f);
}

-(float)calendar:(DACalendar *)calendar rowVerticalSpacingForLayout:(DACalendarLayout *)layout{
    return 2.0f;
}


-(CGSize)calendar:(DACalendar *)calendar dayOfWeekItemSizeForLayout:(DACalendarLayout *)layout{
    return CGSizeMake(25, 20);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"didSelectItemAtIndexPath");
////    [UIView animateWithDuration:2 animations:^{
//    UIImageView *a = [[UIImageView alloc]initWithImage:[self screenShot]];
//    [self.view addSubview:a];
//    a.layer.contents = (id)[UIImage imageBySplittingTop:[self screenShot] pixels:220.0f];
////    }];
}


//- (UIImage *)screenShot {
//    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
//    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return image;
//}








-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    //    [UIView animateWithDuration:.25f animations:^{
    //        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    //        cell.backgroundColor=[UIColor clearColor];
    //    }];
}

#pragma mark -
#pragma mark UIScrollViewDelegate


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UICollectionView class]]) {
        [self animateSnapDown];
    }
}


#pragma mark -
#pragma mark UIDYNAMICS ANIMATIONS


- (void)animateSnapUp {
    [self.mainView bringSubviewToFront:self.categoriesButtonView];
    [self.animator removeAllBehaviors];
    UISnapBehavior *snap;
    snap = [[UISnapBehavior alloc] initWithItem:self.categoriesButtonView snapToPoint:CGPointMake(self.categoriesButtonView.frame.size.width/2, self.categoriesButtonView.frame.size.height+self.categoriesButtonView.frame.size.height/2-BOTTOM_TABLE_HEIGHT)];
    [self.animator addBehavior:snap];
}

- (void)animateSnapDown {
    [self.mainView bringSubviewToFront:self.categoriesButtonView];
    [self.animator removeAllBehaviors];
    UISnapBehavior *snap;
    snap = [[UISnapBehavior alloc] initWithItem:self.categoriesButtonView snapToPoint:CGPointMake(self.categoriesButtonView.frame.size.width/2, self.categoriesButtonView.frame.size.height+self.categoriesButtonView.frame.size.height/2-BOTTOM_CATEGORIES_BUTTON_SIZE)];
    [self.animator addBehavior:snap];
}



#pragma mark -
#pragma mark UITableViewDelegate



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageWithColor:[UIColor grayColor] andSize:CGSizeMake(50, 100)]];
        cell.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageWithColor:[UIColor greenColor] andSize:CGSizeMake(50, 100)]];
    }
    
    if ([self.categoriesArraySelected[indexPath.row]  isEqual:@YES]) {
        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }

    
    
//    UIImageView *checkboxImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,40,40)];
//    [cell addSubview:checkboxImageView];
    cell.textLabel.text = [self.categoriesArray objectAtIndex:indexPath.row];
    [cell setIndentationLevel:3];
    
    return cell;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categoriesArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return TABLE_CELL_HEIGHT;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
       self.categoriesArraySelected[indexPath.row] = @YES;
    [self formEventsDictionary];
    [self.calendar reloadData];
}



- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.categoriesArraySelected[indexPath.row]=@NO;
    [self formEventsDictionary];
    [self.calendar reloadData];
    return indexPath;
}


@end
