//
//  DACalendarLayoutDelegateProtocol.h
//  Calendar
//
//  Created by Alexander Dovlatov on 06.02.14.
//  Copyright (c) 2014 Appreal LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DACalendarDateCell;
@class DACalendar;
@class DACalendarLayout;
@protocol DACalendarDatasourceProtocol <NSObject>

-(UICollectionViewCell*)calendar:(DACalendar*)calendar dateItemAtIndexPath:(NSIndexPath*)indexPath forDate:(NSDate*)date forLayout:(DACalendarLayout*)layout;
-(CGSize)calendar:(DACalendar*)calendar dateItemSizeForLayout:(DACalendarLayout*)layout;


-(UICollectionReusableView*)calendar:(DACalendar*)calendar monthHeaderViewAtIndexPath:(NSIndexPath*)indexPath forDate:(NSDate*)date forLayout:(DACalendarLayout*)layout; //01.month.year



@optional
-(CGSize)calendar:(DACalendar*)calendar monthHeaderViewSizeForLayout:(DACalendarLayout*)layout;
-(UIEdgeInsets)calendar:(DACalendar*)calendar contentInsetsForLayout:(DACalendarLayout*)layout;
-(UIEdgeInsets)calendar:(DACalendar*)calendar headerInsetsForLayout:(DACalendarLayout*)layout;
-(UIEdgeInsets)calendar:(DACalendar*)calendar sectionInsetsForLayout:(DACalendarLayout*)layout;

-(float)calendar:(DACalendar*)calendar rowVerticalSpacingForLayout:(DACalendarLayout*)layout;

@end
